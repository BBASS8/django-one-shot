from django.urls import path
from todos.views import (
    todo_list,
    show_todo_list,
    create_list,
    todo_list_edit,
    todo_list_delete,
)

urlpatterns = [
    path("", todo_list, name="todo_list"),
    path("<int:id>/", show_todo_list, name="show_todo_list"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", todo_list_edit, name="todo_list_edit"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
]
